module "this" {
  source        = "git::https://gitlab.com/echo6/terraform-aws-s3.git?ref=03ebd01be13dd6fb876c66b15e00dc449205003a"
  bucket_prefix = var.bucket_prefix
  public        = var.public
  protection    = var.protection
}

resource "aws_s3_bucket_object" "this" {
  bucket = module.this.id
  key    = var.key
}
